# Gestion d'une bijouterie (Framework PHP codeigniter 3 )
L'Application permet de :
* Ajouter, modifier, supprimer des produits
* Ajouter , modifier, supprimer des clients
* Gerer automatiquement le stock des produits
* Afficher graphiquement la quentité des produits en stock
* Gerer le panier d'achat d'un client lors de l'achat
* Gerer les commandes des clients
* Imprimer une facture


## Prérequis et installation
* L'application est codée sur Framework php Codeignuter
* Elle necessite php version 7 et un serveur apache



## Base de donnees 
* Importer la base "projetci.sql" dans votre base

## Commant lancer l'application?
* Coller le dossier dans votre repertoir www
* lancer l'application

## Authentification Par defaut
* Utilisateur:
```
admin

```
* Mot de passe:
```
admin

```
